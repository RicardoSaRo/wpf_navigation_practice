﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF_Navigation_Practice.Commands;

namespace WPF_Navigation_Practice.ViewModels
{
    public class NavigationViewModel : INotifyPropertyChanged
    {
        private bool _hiddenButton;
        public bool HiddenButton
        {
            get { return _hiddenButton; }
            set
            {
                _hiddenButton = value;
                OnPropertyChanged("HiddenButton");
            }
        }

        public ICommand ClientManagementCommand { get; set; }
        public ICommand MessageTemplatesCommand { get; set; }

        private object _currentViewModel;

        public object CurrentViewModel
        {
            get { return _currentViewModel; }

            set { 
                _currentViewModel = value; 
                OnPropertyChanged("CurrentViewModel"); 
            }
        }

        public NavigationViewModel()
        {
            ClientManagementCommand = new CommandHandler(OpenClientManagement);

            MessageTemplatesCommand = new CommandHandler(OpenMessageTemplates);
        }

        private void OpenClientManagement(object obj)
        {
            HiddenButton = true;
            CurrentViewModel = new ClientManagementViewModel();
        }

        private void OpenMessageTemplates(object obj)
        {
            CurrentViewModel = new MessageTemplatesViewModel();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
